#!/usr/bin/ruby

if ARGV[0] != "EN" and ARGV[0] != "DE"
  STDERR.puts( <<EOS % $0 )
%s LANG
  reads Materials.xml and LANG/names.csv
  writes LANG/localizedStrings.loc
EOS
  exit( 1 )
end

lang = ARGV[0]

# get names
current_name = {}
File.readlines( '%s/names.csv' % lang, chomp: true ).each do |l|
  id, name = l.split( ',' )
  current_name[id] = name
end

# names for current material numbers
File.open( '%s/localizedStrings.loc' % lang, 'wb' ) do |out|
  out.write( "2\000" )
  File.readlines( 'Materials.xml', chomp: true ).
    map.with_index { |l, i| i < 2 ? nil : l.split( '"' )[1] }.
    compact.each do |id|
    out.write( "Material%s\000\%s\000" % [ id, current_name[id] ] )
  end
  out.flush
end
