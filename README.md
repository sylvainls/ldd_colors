# ldd_colors

Colors definitions for LDD / LEGO Digital Designer.

* Materials.xml: defines all the colors
* CurrentMaterials: lists the current colors
* MaterialSelect.xml: lists the colors used in LDD Extended mode
* {DE,EN}/names.csv: human readable color names
* {DE,EN}/localizedStrings.loc: color names for LDD
* mklocfile.rb: little script to make localizedStrings.loc from Materials.xml and names.csv
* LDD.paxml: palette file for LDD “normal” mode with added info
